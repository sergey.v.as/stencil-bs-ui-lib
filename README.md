[ ![Codeship Status for Uiseguys/stencil-bs-ui-lib](https://app.codeship.com/projects/b8adcf50-efd3-0135-29cf-36953f8eb488/status?branch=master)](https://app.codeship.com/projects/271184)

# stencil-bs-ui-lib

## Live Demo
[https://stencil-bs-ui-lib.herokuapp.com/](https://stencil-bs-ui-lib.herokuapp.com/)

## TO Publish on NPM

run command `npm run publish`
before running this command on terminal make sure you are login into npm account
for that run `npm login` form the terminal and provide your credential to do the login authentication